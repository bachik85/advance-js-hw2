//Try...Catch можно использовать при вводе данных в форму,
// допустим пользователь ввел не корректные данные, можно вывести сообщение об ошибке.


const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

function filterBookList() {
  const container = document.getElementById('root');
  const ul = document.createElement('ul');

  container.append(ul);
  ul.style.cssText = "display:inline";

  function ErrorMsg(item, prop) {
    throw new Error(`Incomplete information! ${prop}  book: "${item.name}"`);
  }

  books.forEach(item => {
    try {
      const {
        author,
        name,
        price
      } = item;
      if (!name) {
        ErrorMsg(item, 'No name');
      } else if (!author) {
        ErrorMsg(item, 'No author');
      } else if (!price) {
        ErrorMsg(item, 'No price');
      } else {
        let li = document.createElement('li');
        ul.append(li);
        li.append(`author: ${author}, name: ${name}, price: ${price} $`);
        li.style.cssText = "margin-bottom: 10px; font-size: 24px"
      }
    } catch (err) {
      console.error(err);
    }
  })
}

filterBookList(books);